output "node_config" {
  value       = local.node_config
  sensitive   = true
  description = "Nodes config"
}
