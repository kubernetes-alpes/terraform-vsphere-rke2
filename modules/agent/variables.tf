variable "node_config" {
  type = object({
    bootstrap_server = string
    cluster_name     = string
    datastore_id     = string
    dns_server_list  = list(string)
    domain           = string
    folder           = string
    guest_id         = string
    ipv4_gateway     = string
    ipv4_netmask     = number
    network_id       = string
    pool_id          = string
    rke2_version     = string
    rke2_token       = string
    registries_conf  = string
  })
}

variable "name_prefix" {
  type = string
}

variable "num_cpu" {
  type = number
}

variable "memory" {
  type = number
}

variable "disk_size" {
  type = number
}

variable "template_uuid" {
  type = string
}

variable "ip_list" {
  type = list(string)
}

variable "rke2_config_file" {
  type    = string
  default = ""
}
